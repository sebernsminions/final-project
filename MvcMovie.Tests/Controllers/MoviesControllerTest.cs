﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcMovie.Controllers;
using System.Web.Mvc;

namespace MvcMovie.Tests.Controllers
{
    [TestClass]
    public class MoviesControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            MoviesController controller = new MoviesController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ValidDetails()
        {
            // Arrange
            MoviesController controller = new MoviesController();

            // Act
            ViewResult result = controller.Details() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void InValidDetails()
        {
            // Arrange
            MoviesController controller = new MoviesController();

            // Act
            ViewResult result = controller.Details(-420) as ViewResult;

            // Assert
            Assert.IsNull(result);
        }

        [TestMethod]
        public void GetCreate()
        {
            // Arrange
            MoviesController controller = new MoviesController();

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void PostCreate()
        {

        }

        [TestMethod]
        public void GetEdit()
        {

        }

        [TestMethod]
        public void PostEdit()
        {

        }

        [TestMethod]
        public void GetDelete()
        {

        }

        [TestMethod]
        public void PostDelete()
        {

        }

        [TestMethod]
        public void SearchIndex()
        {

        }
    }
}
