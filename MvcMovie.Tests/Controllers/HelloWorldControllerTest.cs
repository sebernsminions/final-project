﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcMovie.Controllers;
using System.Web.Mvc;

namespace MvcMovie.Tests.Controllers
{
    /// <summary>
    /// Summary description for HelloWorldControllerTest
    /// </summary>
    [TestClass]
    public class HelloWorldControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HelloWorldController controller = new HelloWorldController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.AreEqual("Hello Cruel World", result.ViewBag.Message);
        }

        [TestMethod]
        public void Welcome()
        {
            // Arrange
            HelloWorldController controller = new HelloWorldController();

            // Act
            ViewResult result = controller.Welcome("Andrew", 2) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
