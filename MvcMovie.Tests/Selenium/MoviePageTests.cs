﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace SeleniumTests
{
    [TestFixture]
    public class MoviePageTests
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [TestFixtureSetUp]
        public void SetupTest()
        {
            driver = new ChromeDriver();
            
            baseURL = "http://localhost:58728/";
            verificationErrors = new StringBuilder();
        }

        [TestFixtureTearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void PageAndTableLoadTest()
        {
            driver.Navigate().GoToUrl(baseURL + "/movies");
            Assert.IsNotNull(driver.FindElement(By.Id("movieTable")));
        }

        [Test]
        public void CreateMovieTest()
        {
            driver.Navigate().GoToUrl(baseURL + "/movies");
            driver.FindElement(By.LinkText("Create New")).Click();
            driver.FindElement(By.Id("Title")).Clear();
            driver.FindElement(By.Id("Title")).SendKeys("SE 3800 Demo");
            driver.FindElement(By.Id("ReleaseDate")).Clear();
            driver.FindElement(By.Id("ReleaseDate")).SendKeys("2/25/14");
            driver.FindElement(By.Id("Genre")).Clear();
            driver.FindElement(By.Id("Genre")).SendKeys("Documentary");
            driver.FindElement(By.Id("Price")).Clear();
            driver.FindElement(By.Id("Price")).SendKeys("40");
            driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();
        }

        [Test]
        public void ViewMovieDetailsTest()
        {
            driver.Navigate().GoToUrl(baseURL + "/movies");
            driver.FindElement(By.XPath("(//a[contains(text(),'Details')])[6]")).Click();
            driver.FindElement(By.LinkText("Back to List")).Click();
        }

        [Test]
        public void EditMovieTest()
        {
            driver.Navigate().GoToUrl(baseURL + "/movies");
            driver.FindElement(By.XPath("(//a[contains(text(),'Edit')])[6]")).Click();
            driver.FindElement(By.Id("Title")).Clear();
            driver.FindElement(By.Id("Title")).SendKeys("SE 3800 Demo Edited");
            driver.FindElement(By.Id("ReleaseDate")).Clear();
            driver.FindElement(By.Id("ReleaseDate")).SendKeys("2/23/2014 12:00:00 AM");
            driver.FindElement(By.Id("Genre")).Clear();
            driver.FindElement(By.Id("Genre")).SendKeys("Action");
            driver.FindElement(By.Id("Price")).Clear();
            driver.FindElement(By.Id("Price")).SendKeys("400.00");
            driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();
        }

        [Test]
        public void TheDeleteMovieTest()
        {
            driver.Navigate().GoToUrl(baseURL + "/movies");
            driver.FindElement(By.XPath("(//a[contains(text(),'Delete')])[6]")).Click();
            driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();
        }

        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}
