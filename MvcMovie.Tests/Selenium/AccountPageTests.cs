﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace SeleniumTests
{
    [TestFixture]
    public class SuccessfulLogin
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            driver = new ChromeDriver();
            baseURL = "http://localhost:58728/";
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void UnsuccessfulLoginTest()
        {
            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("loginLink")).Click();
            driver.FindElement(By.Id("UserName")).Clear();
            driver.FindElement(By.Id("UserName")).SendKeys("testUser");
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys("testtestt");
            driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();

            // Verify We're still on the login page
            String actualTitle = driver.Title;
            String expectedTitle = "Log in - Movie App";
            Assert.AreEqual(expectedTitle, actualTitle);
        }

        [Test]
        public void SuccessfulLoginTest()
        {
            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("loginLink")).Click();
            driver.FindElement(By.Id("UserName")).Clear();
            driver.FindElement(By.Id("UserName")).SendKeys("testUser");
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys("testtest");
            driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();

            // Verify We've made it to the home page
            String actualTitle = driver.Title;
            String expectedTitle = "Home Page - Movie App";
            Assert.AreEqual(expectedTitle, actualTitle);
        }

        [Test]
        public void RegisterWithDuplicateUsernameTest()
        {
            driver.Navigate().GoToUrl(baseURL + "/");
            driver.FindElement(By.Id("registerLink")).Click();
            driver.FindElement(By.Id("UserName")).Clear();
            driver.FindElement(By.Id("UserName")).SendKeys("testUser");
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys("testtest");
            driver.FindElement(By.Id("ConfirmPassword")).Clear();
            driver.FindElement(By.Id("ConfirmPassword")).SendKeys("testtest");
            driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();

            // Verify We're still on the register page
            String actualTitle = driver.Title;
            String expectedTitle = "Register - Movie App";
            Assert.AreEqual(expectedTitle, actualTitle);
        }

        [Test]
        public void RegisterForAccountTest()
        {
            //FUTURE: instead of appending a random number and creating a bunch of test users
            //          we should delete the newly created testuser after we've verified they 
            //          successfully registerd
            Random rnd = new Random();
            int idNum = rnd.Next(80000);

            driver.Navigate().GoToUrl(baseURL + "/Account/Login");
            driver.FindElement(By.CssSelector("form > p > a")).Click();
            driver.FindElement(By.Id("UserName")).Clear();
            driver.FindElement(By.Id("UserName")).SendKeys("testUser"+idNum);
            driver.FindElement(By.Id("Password")).Clear();
            driver.FindElement(By.Id("Password")).SendKeys("testtest");
            driver.FindElement(By.Id("ConfirmPassword")).Clear();
            driver.FindElement(By.Id("ConfirmPassword")).SendKeys("testtest");
            driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();

            // Verify We've made it to the home page
            String actualTitle = driver.Title;
            String expectedTitle = "Home Page - Movie App";
            Assert.AreEqual(expectedTitle, actualTitle);
        }

        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}
